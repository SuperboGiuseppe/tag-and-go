const express = require('express');
const path = require('path');
const cookieSession = require('cookie-session');
const bcrypt = require('bcryptjs');
const dbConnection = require('./database');
const { body, validationResult } = require('express-validator');

const app = express();
app.use(express.urlencoded({extended:false}));

// SET OUR VIEWS AND VIEW ENGINE
app.set('views', path.join(__dirname,'views'));
app.set('view engine','ejs');

// SET STATIC ASSETS DIRECTORY
app.use(express.static(__dirname+'/views/'));
// APPLY COOKIE SESSION MIDDLEWARE
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2'],
    maxAge:  3600 * 1000 // 1hr
}));

// DECLARING CUSTOM MIDDLEWARE
const ifNotLoggedin = (req, res, next) => {
    if(!req.session.isLoggedIn){
        return res.render('login-register');
    }
    next();
}

const ifLoggedin = (req,res,next) => {
    if(req.session.isLoggedIn){
        return res.redirect('/');
    }
    next();
}
// END OF CUSTOM MIDDLEWARE

// ROOT PAGE
app.get('/', ifNotLoggedin, (req,res,next) => {
    dbConnection.execute("SELECT `name`, `email` FROM `users` WHERE `id`=?",[req.session.userID])
    .then(([user]) => {
            if(user[0].role==="Business"){
                res.render('dashboard-business',{
                name:user[0].name,
                email:user[0].email});  
            }
            else{
                dbConnection.execute("SELECT * FROM `fridges`").then(([fridges]) => {
                    res.render('dashboard',{
                    name:user[0].name,
                    email:user[0].email,
                    fridges:fridges});
                });
            }
         
        });
    });
// END OF ROOT PAGE



// REGISTER PAGE
app.post('/register', ifLoggedin,
// post data validation(using express-validator)
[
    body('user_email','Invalid email address!').isEmail().custom((value) => {
        return dbConnection.execute('SELECT `email` FROM `users` WHERE `email`=?', [value])
        .then(([rows]) => {
            if(rows.length > 0){
                return Promise.reject('This E-mail already in use!');
            }
            return true;
        });
    }),
    body('user_name','Username is Empty!').trim().not().isEmpty(),
    body('user_pass','The password must be of minimum length 6 characters').trim().isLength({ min: 6 }),
],// end of post data validation
(req,res,next) => {

    const validation_result = validationResult(req);
    const {user_name, user_pass, user_email, user_role} = req.body;
    // IF validation_result HAS NO ERROR
    if(validation_result.isEmpty()){
        // password encryption (using bcryptjs)
        bcrypt.hash(user_pass, 12).then((hash_pass) => {
            // INSERTING USER INTO DATABASE
            dbConnection.execute("INSERT INTO `users`(`name`,`role`,`email`,`password`) VALUES(?,?,?,?)",[user_name, user_role, user_email, hash_pass])
            .then(result => {
                res.redirect('/users');
            }).catch(err => {
                // THROW INSERTING USER ERROR'S
                if (err) throw err;
            });
        })
        .catch(err => {
            // THROW HASING ERROR'S
            if (err) throw err;
        })
    }
    else{
        // COLLECT ALL THE VALIDATION ERRORS
        let allErrors = validation_result.errors.map((error) => {
            return error.msg;
        });
        // REDERING login-register PAGE WITH VALIDATION ERRORS
        res.render('login-register',{
            register_error:allErrors,
            old_data:req.body
        });
    }
});// END OF REGISTER PAGE

// SSH CONNECTION PAGE
app.get('/ssh', function(req, res) {
        res.render('ssh', {
                ssh:req.query
        });
});// END OF SSH CONNECTION PAGE

// INFO CONNECTION PAGE
app.get('/info', function(req, res) {
    res.render('info', {
            info:req.query
    });
});// END OF INFO CONNECTION PAGE


app.get('/dashboard-business', function(req, res) {
    res.render('dashboard-business');
});

// USERS PAGE
app.get('/users', (req,res) => {
    dbConnection.execute("SELECT `id`, `name`, `email`, `role` FROM `users`")
    .then(([users]) => {
           res.render('users',{
           users:users,
           session: req.query
        });
    });

});// END OF ROOT PAGE

// SECURITY CONNECTION PAGE
app.get('/security', function(req, res) {
    res.render('security', {
            session:req.query
    });
});// END OF SECURITY PAGE

// SECURITY CONNECTION PAGE
app.get('/tickets', function(req, res) {
    res.render('tickets', {
            session:req.query
    });
});// END OF SECURITY PAGE

// ADD USER REQUEST
app.post('/adduser', (req,res,next) => {
    const validation_result = validationResult(req);
    const {user_name, user_pass, user_email, user_role} = req.body;
    // IF validation_result HAS NO ERROR
    if(validation_result.isEmpty()){
        // password encryption (using bcryptjs)
        bcrypt.hash(user_pass, 12).then((hash_pass) => {
            // INSERTING USER INTO DATABASE
            dbConnection.execute("INSERT INTO `users`(`name`,`role`,`email`,`password`) VALUES(?,?,?,?)",[user_name, user_role, user_email, hash_pass])
            .then(result => {
                res.redirect('back');
            }).catch(err => {
                // THROW INSERTING USER ERROR'S
                if (err) throw err;
            });
        })
        .catch(err => {
            // THROW HASING ERROR'S
            if (err) throw err;
        })
    }
    else{
        // COLLECT ALL THE VALIDATION ERRORS
        let allErrors = validation_result.errors.map((error) => {
            return error.msg;
        });
        // REDERING login-register PAGE WITH VALIDATION ERRORS
        res.render('login-register',{
            register_error:allErrors,
            old_data:req.body
        });
    }
});// END USER REQUEST


// ADD FRIDGE REQUEST
app.post('/addfridge', function(req,res){
    dbConnection.execute('INSERT INTO `fridges` (`name`,`ip_address`) VALUES (?,?)', [req.body.name, req.body.ip_address]);
    res.redirect('/');
});
// END ADD FRIDGE REQUEST

// LOGIN PAGE
app.post('/', ifLoggedin, [
    body('user_email').custom((value) => {
        return dbConnection.execute('SELECT `email` FROM `users` WHERE `email`=?', [value])
        .then(([rows]) => {
            if(rows.length == 1){
                return true;

            }
            return Promise.reject('Invalid Email Address!');

        });
    }),
    body('user_pass','Password is empty!').trim().not().isEmpty(),
], (req, res) => {
    const validation_result = validationResult(req);
    const {user_pass, user_email} = req.body;
    if(validation_result.isEmpty()){

        dbConnection.execute("SELECT * FROM `users` WHERE `email`=?",[user_email])
        .then(([rows]) => {
            // console.log(rows[0].password);
            bcrypt.compare(user_pass, rows[0].password).then(compare_result => {
                if(compare_result === true){
                    req.session.isLoggedIn = true;
                    req.session.userID = rows[0].id;
                    if(rows[0].role === "Business"){
                        res.redirect('/dashboard-business');
                    }else {
                        res.redirect('/');
                    }
                    
                }
                else{
                    res.render('login-register',{
                        login_errors:['Invalid Password!']
                    });
                }
            })
            .catch(err => {
                if (err) throw err;
            });


        }).catch(err => {
            if (err) throw err;
        });
    }
    else{
        let allErrors = validation_result.errors.map((error) => {
            return error.msg;
        });
        // REDERING login-register PAGE WITH LOGIN VALIDATION ERRORS
        res.render('login-register',{
            login_errors:allErrors
        });
    }
});
// END OF LOGIN PAGE

// LOGOUT
app.get('/logout',(req,res)=>{
    //session destroy
    req.session = null;
    res.redirect('/');
});
// END OF LOGOUT

app.use('/', (req,res) => {
    res.status(404).send('<h1>404 Page Not Found!</h1>');
});

app.listen(3000, () => console.log("Tag&Go Management nodejs webserver is running."));