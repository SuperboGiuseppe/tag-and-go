var SUPPORTLIBRARY = SUPPORTLIBRARY || (function(){
    var _args = {}; // private

    return {
        checkServerStatus : function(url, id) {
            fetch(url, {mode: 'no-cors'}).then(r=>{
                document.getElementById(id).innerHTML = "<img src=assets/img/online.png width=\"25\" height=\"30\"> Online";
            })
            .catch(e=>{
                document.getElementById(id).innerHTML = "<img src=assets/img/offline.png width=\"25\" height=\"30\"> Offline";
            });
        }
        };
}());