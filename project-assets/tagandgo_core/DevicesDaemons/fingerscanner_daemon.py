#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" _____________________________________________________________
    Description: Fingerscanner Daemon
    Authors: Giuseppe Superbo (giuseppe.superbo@studenti.unitn.it), Mirko Ioris (mirko.ioris@studenti.unitn.it)
    Date: A.Y. 2020-2021
    Course: ICT Innovation
    Project: Tag & Go
    _____________________________________________________________
"""

"""
Forked from PyFingerprint example
Copyright (C) 2015 Bastian Raschke <bastian.raschke@posteo.de>
All rights reserved.
"""

import hashlib
from pyfingerprint.pyfingerprint import PyFingerprint
from pyfingerprint.pyfingerprint import FINGERPRINT_CHARBUFFER1
from daemon import runner
import support_functions



## Tries to initialize the sensor
class fingerprint_scanner():
    def __init__(self):
        """
         
        """
        self.stdin_path = '/var/log/tagandgo/fingerprint_scanner'
        self.stdin_path = '/var/log/tagandgo/fingerprint_scanner'
        self.stderr_path = '/var/log/tagandgo/fingerprint_scanner'
        self.pidfile_path = '/tmp/fingerprint_scanner.pid'
        self.pidfile_timeout = 5
        
        self.fingerprint_device = self.device_connection()
    
    def device_connection(self):
        try:
            f = PyFingerprint('/dev/ttyS0', 57600, 0xFFFFFFFF, 0x00000000)
            if ( f.verifyPassword() == False ):
                support_functions.refresh_interface_error("/etc/tagandgo/tag-and-go/project-assets/WebServerAssets/UserInterface_WebServer/Html_assets/error.html", "0x0008", "Wrong fingerprint scanner password!")
        except Exception as e:
            support_functions.refresh_interface_error("/etc/tagandgo/tag-and-go/project-assets/WebServerAssets/UserInterface_WebServer/Html_assets/error.html", "0x0009", "Fingerprint scanner failed to boot up! Exception message:"  + str(e))
            print('Exception message: ' + str(e))
            exit(1)

    def run(self):
        while True:
            ## Tries to search the finger and calculate hash
            try:
            ## Wait that finger is read
                while ( self.fingerprint_device.readImage() == False ):
                    pass

                    ## Converts read image to characteristics and stores it in charbuffer 1
                    self.fingerprint_device.convertImage(FINGERPRINT_CHARBUFFER1)

                    ## Searchs template
                    result = self.fingerprint_device.searchTemplate()

                    positionNumber = result[0]
                    accuracyScore = result[1]

                    if ( positionNumber == -1 ):
                        support_functions.refresh_interface_error("/etc/tagandgo/tag-and-go/project-assets/WebServerAssets/UserInterface_WebServer/Html_assets/error.html", "0x0010", "Your finger was not matched. Please try again!")
                        continue
                    elif ( accuracyScore < 50 ):
                        support_functions.refresh_interface_error("/etc/tagandgo/tag-and-go/project-assets/WebServerAssets/UserInterface_WebServer/Html_assets/error.html", "0x0010", "Your finger was not matched. Please try again!")
                        continue
                    else:
                        support_functions.unlock_door()
                        support_functions.refresh_interface("/etc/tagandgo/tag-and-go/project-assets/WebServerAssets/UserInterface_WebServer/Html_assets/shopping_session.html")

            except Exception as e:
                support_functions.refresh_interface_error("/etc/tagandgo/tag-and-go/project-assets/WebServerAssets/UserInterface_WebServer/Html_assets/error.html", "0x0011", "There was a problem with the scanner! Exception message:"  + str(e))
                exit(1)


fingerprint_scanner = fingerprint_scanner.pn532_daemon()
daemon_runner = runner.DaemonRunner(fingerprint_scanner)
daemon_runner.do_action()