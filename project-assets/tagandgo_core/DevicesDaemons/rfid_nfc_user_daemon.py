""" _____________________________________________________________
    Description: RFID NFC PN532 Hat Daemon
    Authors: Giuseppe Superbo (giuseppe.superbo@studenti.unitn.it), Mirko Ioris (mirko.ioris@studenti.unitn.it)
    Date: A.Y. 2020-2021
    Course: ICT Innovation
    Project: Tag & Go
    _____________________________________________________________
"""
import binascii
from database_connection import connect_database
import time
import sys
import support_functions
import database_connection
import mysql.connector
import Adafruit_PN532 as PN532
from daemon import runner


class pn532_deamon():
    def __init__(self, CS, MOSI, MISO, SCLK):
        """
         
        """
        self.stdin_path = '/var/log/tagandgo/pn532'
        self.stdin_path = '/var/log/tagandgo/pn532'
        self.stderr_path = '/var/log/tagandgo/pn532'
        self.pidfile_path = '/tmp/pn532.pid'
        self.pidfile_timeout = 5

        # Number of seconds to delay after reading data.
        self.delay = 3
        # Prefix, aka header from the card
        self.card_header = b'BG'
        self.card_key=[0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]

        self.CS = CS
        self.MOSI = MOSI
        self.MISO = MISO
        self.SCLK = SCLK
        self.pn532 = PN532.PN532(cs=self.CS, sclk=self.SCLK, mosi=self.MOSI, miso=self.MISO)
        self.device_connection()

    def device_connection(self):
        self.pn532.begin()
        self.pn532.SAM_configuration()
        try:
            self.pn532.firmware_version()
        except RuntimeError as e:
            if e.message == "Failed to detect the PN532":
                support_functions.refresh_interface_error("/etc/tagandgo/tag-and-go/project-assets/WebServerAssets/UserInterface_WebServer/Html_assets/error.html", "0x0003", "RFID/NFC Scanner not available")
                sys.exit(0)

    def run(self):
        while True:
            # Wait for a card to be available
            uid = self.pn532.read_passive_target()
            # Try again if no card found
            if uid is None:
                continue
            # Found a card, now try to read block 4 to detect the block type
            print('')
            print('Card UID 0x{0}'.format(binascii.hexlify(uid)))
            # Authenticate and read block 4
            if not self.pn532.mifare_classic_authenticate_block(uid, 4, PN532.MIFARE_CMD_AUTH_B,
                                                           self.CARD_KEY):
                support_functions.refresh_interface_error("/etc/tagandgo/tag-and-go/project-assets/WebServerAssets/UserInterface_WebServer/Html_assets/error.html", "0x0004", "RFID/NFC Tag is not valid!")
                continue
            data = self.pn532.mifare_classic_read_block(4)
            if data is None:
                support_functions.refresh_interface_error("/etc/tagandgo/tag-and-go/project-assets/WebServerAssets/UserInterface_WebServer/Html_assets/error.html", "0x0005", "Failed to read data from card, please touch the icon and retry.")
                continue
            # Check the header
            if data[0:2] !=  self.card_header:
                support_functions.refresh_interface_error("/etc/tagandgo/tag-and-go/project-assets/WebServerAssets/UserInterface_WebServer/Html_assets/error.html", "0x0006", "RFID/NFC Tag data is corrupted or not valid!")
                continue
            # Parse out the block type and subtype
            try:
                database = database_connection.connect_database("internal_db")
                result = database_connection.run_query("SELECT * FROM \`customers\` WHERE ID=" + format(int(data[2:8].decode("utf-8"), 16)))
                if result.rowcount == 0:
                   support_functions.refresh_interface_error("/etc/tagandgo/tag-and-go/project-assets/WebServerAssets/UserInterface_WebServer/Html_assets/error.html", "0x0007", "Customer does not exist!")
                else:
                    support_functions.unlock_door()
                    support_functions.refresh_interface("/etc/tagandgo/tag-and-go/project-assets/WebServerAssets/UserInterface_WebServer/Html_assets/shopping_session.html")
                database_connection.close_connection(database)
            except RuntimeError as err:
                support_functions.refresh_interface_error("/etc/tagandgo/tag-and-go/project-assets/WebServerAssets/UserInterface_WebServer/Html_assets/error.html", "0x0008", format(err))
            time.sleep(self.delay)


    

rfid_nfc_hat = pn532_deamon.pn532_daemon(18,19,21,25)
daemon_runner = runner.DaemonRunner(rfid_nfc_hat)
daemon_runner.do_action()