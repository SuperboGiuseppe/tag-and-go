
""" _____________________________________________________________
    Description: Database connector
    Authors: Giuseppe Superbo (giuseppe.superbo@studenti.unitn.it), Mirko Ioris (mirko.ioris@studenti.unitn.it)
    Date: A.Y. 2020-2021
    Course: ICT Innovation
    Project: Tag & Go
    _____________________________________________________________
"""



import mysql.connector


def connect_database(selected_database):
    mydb = mysql.connector.connect(
        host="localhost",
        user="fridge",
        password="test_password",
        database=selected_database
    )
    return mydb

def run_query(mydb, query):
    mycursor = mydb.cursor()
    mycursor.execute(query)
    return mycursor

def close_connection(mydb):
    mydb.close()

