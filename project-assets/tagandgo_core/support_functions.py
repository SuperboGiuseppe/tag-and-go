""" _____________________________________________________________
    Description: Support functions
    Authors: Giuseppe Superbo (giuseppe.superbo@studenti.unitn.it), Mirko Ioris (mirko.ioris@studenti.unitn.it)
    Date: A.Y. 2020-2021
    Course: ICT Innovation
    Project: Tag & Go
    _____________________________________________________________
"""

import os
import RPi.GPIO as GPIO

relay = 18
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setuprelay(GPIO.OUT)
GPIO.output(relay , 0)

def refresh_interface_error(path_html, error_code, text):
    os.system("sed -i 's/Error:/Error:" + error_code + ":" + text + "/' " + path_html)
    os.system('ln -s ' + path_html + ' /etc/tagandgo/tag-and-go/project-assets/WebServerAssets/UserInterface_WebServer/Html_assets/index.html')
    os.system('WID=$(xdotool search --onlyvisible --class chromium|head -1); xdotool windowactivate ${WID}; xdotool key ctrl+F5') 

def refresh_interface(path_html, error_code, text):
    os.system('ln -s ' + path_html + ' /etc/tagandgo/tag-and-go/project-assets/WebServerAssets/UserInterface_WebServer/Html_assets/index.html')
    os.system('WID=$(xdotool search --onlyvisible --class chromium|head -1); xdotool windowactivate ${WID}; xdotool key ctrl+F5') 


def unlock_door():
    GPIO.output(relay, 1)

def lock_door():
    GPIO.output(relay, 0)
